﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SignalHub.Components
{
    public abstract class BaseComponent : ComponentBase
    {
        private bool HasImplementedIsLoading = true;
        private bool SendingStateHasChanged = false;

        protected virtual void OnAfterLoading() { }

        protected override Task OnAfterRenderAsync(bool firstRender)
        {
            if (!IsLoading() && !SendingStateHasChanged)
            {
                if (HasImplementedIsLoading)
                {
                    OnAfterLoading();
                    SendingStateHasChanged = true;
                    StateHasChanged();
                }
            }
            return base.OnAfterRenderAsync(firstRender);
        }

        protected virtual bool IsLoading()
        {
            HasImplementedIsLoading = false;
            return false;
        }
    }
}
