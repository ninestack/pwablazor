﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SignalHub.Components.Visuals.EventArgs.Table
{
    public class OnRowSelectedEventArgs
    {
        public int Index { get; set; }
        public dynamic Data { get; set; }
    }

    public class OnRowsSelectedEventArgs
    {
        public IEnumerable<int> Indices { get; set; }
        public dynamic Data { get; set; }
    }
}
