﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SignalHub.Components.Extensions;
using SignalHub.Shared.Extensions;
using SignalHub.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SignalHub.Components.Visuals.EventArgs.Table;

namespace SignalHub.Components.Visuals.Base
{
    public abstract class Table : Component
    {
        [Parameter] public EventCallback<OnColumnMovedEventArgs> OnColumnMoved { get; set; }
        [Parameter] public EventCallback<OnColumnResizedEventArgs> OnColumnResized { get; set; }
        [Parameter] public EventCallback<OnCellClickedEventArgs> OnCellClicked { get; set; }
        [Parameter] public EventCallback<OnRowSelectedEventArgs> OnRowSelected { get; set; }
        [Parameter] public EventCallback<OnRowsSelectedEventArgs> OnRowsSelected { get; set; }


        public override async Task AddDimension(FieldDimension field)
        {
            if (ShowingAllFields())
                RemoveAllFromAppearance();

            SetNextResultName(field);
            Visual.Config.DimensionFields = Visual.Config.DimensionFields.WithToList(l => l.Add(field));
            await Reload();
        }

        public override async Task AddMeasure(FieldMeasure field)
        {
            // If any measures exists already add to measures
            // If measure is already added to dimension then remove from dimension and make it a measure
            if (!Visual.Config.MeasureFields.Any())
            {
                var dimensionAddedField = Visual.Config.DimensionFields.FirstOrDefault(d => d.Name == field.Name);
                if (dimensionAddedField == null)
                {
                    var dimensionField = FieldDimension.CreateFrom(field);
                    await AddDimension(dimensionField);
                    return;
                }
                else
                    Visual.Config.DimensionFields = Visual.Config.DimensionFields.WithToList(l => l.Remove(dimensionAddedField));
            }
            SetNextResultName(field);
            Visual.Config.MeasureFields = Visual.Config.MeasureFields.WithToList(l => l.Add(field));
            await Reload();
        }

        public override async Task RemoveDimension(FieldDimension field)
        {
            Visual.Config.DimensionFields = Visual.Config.DimensionFields.WithToList(l => l.Remove(field));
            Visual.Config.OrderFields = Visual.Config.OrderFields.WithToList(l => l.RemoveAll(o => o.ResultName == field.ResultName));
            RemoveFromAppearance(field.ResultName);
            await Reload();
        }

        public override async Task RemoveMeasure(FieldMeasure field) 
        {
            Visual.Config.MeasureFields = Visual.Config.MeasureFields.WithToList(l => l.Remove(field));
            Visual.Config.OrderFields = Visual.Config.OrderFields.WithToList(l => l.RemoveAll(o => o.ResultName == field.ResultName));
            RemoveFromAppearance(field.ResultName);
            await Reload();
        }

        protected void RemoveFromAppearance(string fieldNameToRemove)
        {
            var tableAppearance = Visual.Appearance as TableAppearance;
            if (tableAppearance != null)
                tableAppearance.TableFields = tableAppearance.TableFields.WithToList(l => l.RemoveAll(o => o.Name == fieldNameToRemove));
        }

        protected void RemoveAllFromAppearance()
        {
            var tableAppearance = Visual.Appearance as TableAppearance;
            if (tableAppearance != null)
                tableAppearance.TableFields = tableAppearance.TableFields.WithToList(l => l.Clear());
        }

        protected void SetFieldAppearance(IEnumerable<DrillField> fields)
        {
            var tableAppearance = Visual.Appearance as TableAppearance;
            var index = 0;
            foreach(var field in fields)
                if (tableAppearance.TableFields.FirstOrDefault(f => f.Name == field.ResultName) == null)
                    tableAppearance.TableFields.Add(new TableFieldAppearance { Name = field.ResultName, Index = index++ });
        }

        protected override bool CanLoadData() 
        {
            var hasDimensions = Visual.Config.DimensionFields.Any();
            var hasMeasures = Visual.Config.MeasureFields.Any();
            return hasDimensions || (!hasDimensions && !hasMeasures);
        }

        protected override async Task<object> LoadData(IEnumerable<FieldFilter> additionalFilters = null, string searchText = null)
        {
            var payload = await CreatePayload(additionalFilters, searchText);
            var data = Visual.Config.MeasureFields.Any() ? await Loader.Aggregate<object>(payload) : await Loader.Tabular<object>(payload);
            return PrepareData(data);
        }

        protected override async Task ClearData()
        {
            var data = new List<object>();
            await RenderData(data);
        }

        protected async Task<object> CreatePayload(IEnumerable<FieldFilter> additionalFilters = null, string searchText = null)
        {
            var size = 10;
            if (ShowingAllFields())
            {
                var source = await GetVisualSource();
                var dimensionFields = CreateDimensions(source);
                SetFieldAppearance(dimensionFields);
                return Shared.Services.Payloads.TabularPayload.CreateFrom(dimensionFields, size, Visual.Config.Filters, Visual.Config.OrderFields, Visual.Config.Variables, additionalFilters, searchText);
            }
            else
            {
                if (Visual.Config.MeasureFields.Count() == 0)
                    return Shared.Services.Payloads.TabularPayload.CreateFrom(Visual.Config, size, additionalFilters, searchText);
                else
                    return Shared.Services.Payloads.AggregationPayload.CreateFrom(Visual.Config, size, additionalFilters, searchText);
            }
        }

        protected IEnumerable<FieldDimension> CreateDimensions(Source source)
        {
            const string fieldPrefix = "key";
            var dimensionFields = new List<FieldDimension>();
            foreach (var field in source.Fields)
            {
                var dimensionField = new FieldDimension { Name = field.Name, DataType = field.DataType, ResultName = GetNextResultName(dimensionFields, fieldPrefix) };
                dimensionFields.Add(dimensionField);
            }
            return dimensionFields;
        }

        protected bool ShowingAllFields()
        {
            return Visual.Config.DimensionFields.Count() == 0 && Visual.Config.MeasureFields.Count() == 0;
        }

        protected void EnsureAllFields()
        {
            var tableAppearance = Visual.Appearance as TableAppearance;
            var index = 0;
            var drillFields = new List<DrillField>().AddMany(Visual.Config.DimensionFields, Visual.Config.MeasureFields);
            foreach (var drillField in drillFields)
            {
                var field = tableAppearance.TableFields.FirstOrDefault(f => f.Name == drillField.ResultName);
                if (field == null)
                {
                    field = new TableFieldAppearance { Name = drillField.ResultName };
                    tableAppearance.TableFields.Add(field);
                }
                field.Index = index++;
            }
        }

        [JSInvokable]
        public virtual Task OnMovedColumn(string fieldResultName, int movedToIndex)
        {
            EnsureAllFields();
            // order dimension and measure fields
            //Rearrange();
            // move
            // order appearance fields
            var tableAppearance = Visual.Appearance as TableAppearance;
            var fieldToMove = tableAppearance.TableFields.FirstOrDefault(f => f.Name == fieldResultName);
            tableAppearance.TableFields.Remove(fieldToMove);
            tableAppearance.TableFields.Insert(movedToIndex, fieldToMove);
            int i = 0;
            tableAppearance.TableFields.ToList().ForEach(f => f.Index = i++);
            tableAppearance.TableFields = tableAppearance.TableFields.OrderBy(f => f.Index).ToList();

            var eventArgs = new OnColumnMovedEventArgs { FieldResultName = fieldResultName, MovedToIndex = movedToIndex };
            return OnColumnMoved.InvokeAsync(eventArgs);
        }

        [JSInvokable]
        public virtual Task OnSelectedRow(int index, System.Text.Json.JsonElement data)
        {
            var raw = GetDictionaryFromJsonElement(data);
            var translated = new Dictionary<string, object>();
            //var source = await GetVisualSource();
            foreach (var keyValue in raw)
            {
                var visualFieldName = TranslateResultNameToFieldName(keyValue.Key);
                translated.Add(visualFieldName, keyValue.Value);
            }
            var eventArgs = new OnRowSelectedEventArgs { Index = index, Data = translated.ToExpando() };
            return OnRowSelected.InvokeAsync(eventArgs);
        }

        [JSInvokable]
        public virtual Task OnClickedCell(string fieldResultName, int rowIndex, System.Text.Json.JsonElement value)
        {
            var v = GetValueFromJsonElement(value);
            var eventArgs = new OnCellClickedEventArgs { FieldResultName = fieldResultName, RowIndex = rowIndex, Value = v  };
            return OnCellClicked.InvokeAsync(eventArgs);
        }

        [JSInvokable]
        public virtual Task OnResizedColumn(string fieldResultName, int width)
        {
            var tableAppearance = Visual.Appearance as TableAppearance;
            var field = tableAppearance.TableFields.FirstOrDefault(f => f.Name == fieldResultName);
            if (field == null)
            {
                field = new TableFieldAppearance { Name = fieldResultName };
                tableAppearance.TableFields.Add(field);
            }
            field.CellWidth = width;

            var eventArgs = new OnColumnResizedEventArgs { FieldResultName = fieldResultName, Width = width };
            return OnColumnResized.InvokeAsync(eventArgs);
        }

        public virtual Task SetSelectedRow(int index)
        {
            var eventArgs = new OnRowSelectedEventArgs { Index = index };
            return OnRowSelected.InvokeAsync(eventArgs);
        }

        public virtual Task SetSelectedRows(IEnumerable<int> indices)
        {
            var eventArgs = new OnRowsSelectedEventArgs { Indices = indices };
            return OnRowsSelected.InvokeAsync(eventArgs);
        }

        public virtual string GetSelectedTheme()
        {
            var tableAppearance = Visual.Appearance as TableAppearance;
            return tableAppearance.ThemeName;
        }
    }
}
