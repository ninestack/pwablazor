﻿using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SignalHub.Components.Extensions
{
    public abstract class ElementReferenceJS
    {
        protected ElementReference ElementRef;
        protected IJSRuntime JSRuntime;
        public ElementReferenceJS(ElementReference elementRef, IJSRuntime jsRuntime)
        {
            ElementRef = elementRef;
            JSRuntime = jsRuntime;
        }
    }

    public abstract class AmchartsJS : ElementReferenceJS
    {
        public AmchartsJS(ElementReference elementRef, IJSRuntime jsRuntime) : base(elementRef, jsRuntime) { }

        public abstract Task Create(object context);

        public virtual async Task CreateCategoryAxis(object context)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.createCategoryAxisByRef", ElementRef, context);
        }
        public virtual async Task ReplaceCategoryAxis(object context)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.replaceCategoryAxisByRef", ElementRef, context);
        }
        public virtual async Task RemoveCategoryAxis(object context)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.removeCategoryAxisByRef", ElementRef, context);
        }
        public virtual async Task CreateValueAxis(object context)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.createValueAxisByRef", ElementRef, context);
        }
        public virtual async Task CreateSeries(object context)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.createSeriesByRef", ElementRef, context);
        }
        public virtual async Task CreateSerieses(object context)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.createSeriesesByRef", ElementRef, context);
        }
        public virtual async Task RemoveSeries(object context)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.removeSeriesByRef", ElementRef, context);
        }
        public virtual async Task RemoveSerieses()
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.removeSeriesesByRef", ElementRef);
        }
        public virtual async Task SetData(object context)
        {
            await JSRuntime.InvokeAsync<object>("AmChartEngine.loadDataByRef", ElementRef, context);
        }
        public virtual async Task ShowLoadingIndicator()
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.showIndicatorByRef", ElementRef);
        }
        public virtual async Task HideLoadingIndicator()
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.hideIndicatorByRef", ElementRef);
        }
        public async Task UpdateSettings(object settings)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.updateSettingsByRef", ElementRef, settings);
        }
    }

    public class ColumnAmchartJS : AmchartsJS
    {
        public ColumnAmchartJS(ElementReference elementRef, IJSRuntime jsRuntime) : base(elementRef, jsRuntime) { }
        public override async Task Create(object context)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.createColumnChartByRef", ElementRef, context);
        }
    }
    public class BarAmchartJS : ColumnAmchartJS
    {
        public BarAmchartJS(ElementReference elementRef, IJSRuntime jsRuntime) : base(elementRef, jsRuntime) { }
        public override async Task Create(object context)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.createBarChartByRef", ElementRef, context);
        }
    }
    public class TimeAmchartJS : ColumnAmchartJS
    {
        public TimeAmchartJS(ElementReference elementRef, IJSRuntime jsRuntime) : base(elementRef, jsRuntime) { }
        public override async Task Create(object context)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.createTimeChartByRef", ElementRef, context);
        }
    }
    public class PieAmchartJS : ColumnAmchartJS
    {
        public PieAmchartJS(ElementReference elementRef, IJSRuntime jsRuntime) : base(elementRef, jsRuntime) { }
        public override async Task Create(object context)
        {
            await JSRuntime.InvokeVoidAsync("AmChartEngine.createPieChartByRef", ElementRef, context);
        }
    }


    public class LeafletMapJS : ElementReferenceJS
    {
        public LeafletMapJS(ElementReference elementRef, IJSRuntime jsRuntime) : base(elementRef, jsRuntime) { }
        public virtual async Task Create(object context)
        {
            await JSRuntime.InvokeVoidAsync("LeafletMapEngine.createMapByRef", ElementRef, context);
        }
        public virtual async Task ZoomMap(object context)
        {
            await JSRuntime.InvokeVoidAsync("LeafletMapEngine.zoomMapByRef", ElementRef, context);
        }
        public virtual async Task LayerMap(object context)
        {
            await JSRuntime.InvokeVoidAsync("LeafletMapEngine.layerMapByRef", ElementRef, context);
        }
        public virtual async Task ExportMap(object context)
        {
            await JSRuntime.InvokeVoidAsync("LeafletMapEngine.exportMapByRef", ElementRef, context);
        }
        public virtual async Task ResetMap(object context)
        {
            await JSRuntime.InvokeVoidAsync("LeafletMapEngine.resetMapByRef", ElementRef, context);
        }
        public virtual async Task AppendPolygons(object context)
        {
            await JSRuntime.InvokeVoidAsync("LeafletMapEngine.appendPolygonsByRef", ElementRef, context);
        }
        public virtual async Task ClearPolygons(object context)
        {
            await JSRuntime.InvokeVoidAsync("LeafletMapEngine.clearPolygonsByRef", ElementRef, context);
        }
        public virtual async Task ClearPolygons()
        {
            await JSRuntime.InvokeVoidAsync("LeafletMapEngine.clearPolygonsByRef", ElementRef);
        }
        public virtual async Task UpdatePolygons(object context)
        {
            await JSRuntime.InvokeVoidAsync("LeafletMapEngine.updatePolygonsByRef", ElementRef, context);
        }
        public virtual async Task RedirectToNewTab(string url)
        {
            await JSRuntime.InvokeVoidAsync("RedirectToNewTab", url);
        }
        public async Task UpdateSettings(object settings)
        {
            throw new NotImplementedException();
            //await JSRuntime.InvokeVoidAsync("", ElementRef, settings);
        }

        public virtual async Task AppendMarkers(object context)
        {
            await JSRuntime.InvokeVoidAsync("LeafletMapEngine.appendMarkersByRef", ElementRef, context);
        }
        public virtual async Task AppendMarker(double lat, double lng)
        {
            await JSRuntime.InvokeVoidAsync("LeafletMapEngine.appendMarkerByRef", ElementRef, lat, lng);
        }
    }

    public class GridStackJS : ElementReferenceJS
    {
        public GridStackJS(ElementReference elementRef, IJSRuntime jsRuntime) : base(elementRef, jsRuntime) { }
        public async Task Layout(object context)
        {
            await JSRuntime.InvokeVoidAsync("layoutGridStackByRef", ElementRef, context);
        }
        public async Task UpdateSettings(object settings)
        {
            throw new NotImplementedException();
            //await JSRuntime.InvokeVoidAsync("", ElementRef, settings);
        }
    }

    public class agGridJS : ElementReferenceJS
    {
        public agGridJS(ElementReference elementRef, IJSRuntime jsRuntime) : base(elementRef, jsRuntime) { }
        public async Task Create(object context)
        {
            await JSRuntime.InvokeVoidAsync("agGridEngine.createGridByRef", ElementRef, context);
        }
        public async Task UpdateContext(object context)
        {
            await JSRuntime.InvokeVoidAsync("agGridEngine.updateContextByRef", ElementRef, context);
        }
        public async Task SetColumns(object columns)
        {
            await JSRuntime.InvokeVoidAsync("agGridEngine.setColumnsByRef", ElementRef, columns);
        }
        public async Task SetData(object data)
        {
            await JSRuntime.InvokeVoidAsync("agGridEngine.setDataByRef", ElementRef, data);
        }
        public async Task SetSelectedRow(int index)
        {
            await JSRuntime.InvokeVoidAsync("agGridEngine.setSelectedRow", ElementRef, index);
        }
        public async Task SetSelectedRows(IEnumerable<int> indices)
        {
            await JSRuntime.InvokeVoidAsync("agGridEngine.setSelectedRows", ElementRef, indices);
        }
        public async Task UpdateSettings(object settings)
        {
            throw new NotImplementedException();
            //await JSRuntime.InvokeVoidAsync("", ElementRef, settings);
        }
        public async Task ShowNoRows()
        {
            await JSRuntime.InvokeVoidAsync("agGridEngine.showNoRowsOverlayByRef", ElementRef);
        }
        public async Task ShowLoader()
        {
            await JSRuntime.InvokeVoidAsync("agGridEngine.showLoaderOverlayByRef", ElementRef);
        }
        public async Task HideLoader()
        {
            await JSRuntime.InvokeVoidAsync("agGridEngine.hideOverlayByRef", ElementRef);
        }
    }


    public static class ElementReferenceExtension
    {
        public static ColumnAmchartJS ColumnAmchartJS(this ElementReference elementRef, IJSRuntime jsRuntime)
        {
            return new ColumnAmchartJS(elementRef, jsRuntime);
        }

        public static BarAmchartJS BarAmchartJS(this ElementReference elementRef, IJSRuntime jsRuntime)
        {
            return new BarAmchartJS(elementRef, jsRuntime);
        }

        public static TimeAmchartJS TimeAmchartJS(this ElementReference elementRef, IJSRuntime jsRuntime)
        {
            return new TimeAmchartJS(elementRef, jsRuntime);
        }

        public static PieAmchartJS PieAmchartJS(this ElementReference elementRef, IJSRuntime jsRuntime)
        {
            return new PieAmchartJS(elementRef, jsRuntime);
        }

        public static LeafletMapJS LeafletMapJS(this ElementReference elementRef, IJSRuntime jsRuntime)
        {
            return new LeafletMapJS(elementRef, jsRuntime);
        }

        public static GridStackJS GridStackJS(this ElementReference elementRef, IJSRuntime jsRuntime)
        {
            return new GridStackJS(elementRef, jsRuntime);
        }

        public static agGridJS agGridJS(this ElementReference elementRef, IJSRuntime jsRuntime)
        {
            return new agGridJS(elementRef, jsRuntime);
        }




        //public static async Task CreateColumnChart(this ElementReference elementRef, IJSRuntime jsRuntime, object context)
        //{
        //    await jsRuntime.InvokeVoidAsync("AmChartEngine.createColumnChart", elementRef, context);
        //}

        //public static async Task CreateBarChart(this ElementReference elementRef, IJSRuntime jsRuntime, object context)
        //{
        //    await jsRuntime.InvokeVoidAsync("AmChartEngine.createBarChart", elementRef, context);
        //}

        public static async Task CreatePieChart(this ElementReference elementRef, IJSRuntime jSRuntime, object context)
        {
            await jSRuntime.InvokeVoidAsync("AmChartEngine.createPieChart", elementRef, context);
        }

        public static async Task CreateTimeChart(this ElementReference elementRef, IJSRuntime jSRuntime, object context)
        {
            await jSRuntime.InvokeVoidAsync("AmChartEngine.createTimeChart", elementRef, context);
        }

        public static async Task CreateMap(this ElementReference elementRef, IJSRuntime jSRuntime, object context)
        {
            await jSRuntime.InvokeVoidAsync("LeafletMapEngine.createMap", elementRef, context);
        }

        public static async Task AppendPolygons(this ElementReference elementRef, IJSRuntime jSRuntime, object context)
        {
            await jSRuntime.InvokeVoidAsync("LeafletMapEngine.appendPolygons", elementRef, context);
        }

        public static async Task SetTitle(this ElementReference elementRef, IJSRuntime jsRuntime, string title)
        {
            await jsRuntime.InvokeAsync<object>("AmChartEngine.setTitle", elementRef, title);
        }

        public static async Task SetXAxisTitle(this ElementReference elementRef, IJSRuntime jsRuntime, string title)
        {
            await jsRuntime.InvokeAsync<object>("AmChartEngine.setXAxisTitle", elementRef, title);
        }

        public static async Task SetYAxisTitle(this ElementReference elementRef, IJSRuntime jsRuntime, string title)
        {
            await jsRuntime.InvokeAsync<object>("AmChartEngine.setYAxisTitle", elementRef, title);
        }

        public static async Task AddValueAxis(this ElementReference elementRef, IJSRuntime jsRuntime, object context)
        {
            await jsRuntime.InvokeAsync<object>("AmChartEngine.createValueAxisByRef", elementRef);
        }


        public static async Task SetTimeData(this ElementReference elementRef, IJSRuntime jsRuntime, object context)
        {
            await jsRuntime.InvokeAsync<object>("AmChartEngine.loadTimeData", elementRef, context);
        }

        public static async Task SetData(this ElementReference elementRef, IJSRuntime jsRuntime, object context)
        {
            await jsRuntime.InvokeAsync<object>("AmChartEngine.loadData", elementRef, context);
        }

        public static async Task ModalShow(this ElementReference elementRef, IJSRuntime jsRuntime)
        {
            await jsRuntime.InvokeAsync<object>("Modal.show", elementRef);
        }

        public static async Task ModalHide(this ElementReference elementRef, IJSRuntime jsRuntime)
        {
            await jsRuntime.InvokeAsync<object>("Modal.hide", elementRef);
        }
    }
}
