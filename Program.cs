using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using PWA.Shared;
using Kundkartan.Shared.Extensions;
using SignalHub.Shared.Extensions;
using Microsoft.Extensions.Configuration;
using System.Diagnostics;

namespace PWA_Blazor
{
    public class Program
	{
        public static IConfiguration Configuration { get; set; }
        public static async Task Main(string[] args)
		{
			var builder = WebAssemblyHostBuilder.CreateDefault(args);
			builder.RootComponents.Add<App>("app"); //"app" refers to a selector on index.html

            builder.Services.AddBaseAddressHttpClient();
			builder.Services.AddSingleton<QueryService>();
            //ConfigureServices(builder.Services);
            //builder.Services.AddSingleton(s =>
            //{
            //    // Get the service address from appsettings.json
            //    Configuration = s.GetRequiredService<IConfiguration>();
            //    var settings = Configuration["ApplicationSettings"];
            //    Debug.WriteLine(settings);
            //    return settings;

            //});
            var host = builder.Build();
            Configuration = host.Services.GetRequiredService<IConfiguration>();
            Debug.WriteLine("Configuration: " + Configuration);
            var applicationSettingsSection = Configuration.GetSection("ApplicationSettings");
            builder.Services.Configure<SignalHub.Shared.Settings.Application>(applicationSettingsSection);
            var isDevEnv = (Environment.GetEnvironmentVariables().Contains("ASPNETCORE_ENVIRONMENT"))
                ? string.Equals(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"), "Development", StringComparison.OrdinalIgnoreCase)
                : default;

            // SignalHub general
            if (!isDevEnv) builder.Services.AddSignalHub("SignalHub", applicationSettingsSection);

            //builder.Services.AddRazorPages();
            //builder.Services.AddServerSideBlazor().AddHubOptions(o =>
            //{
            //    o.MaximumReceiveMessageSize = 10 * 1024 * 1024;
            //});
            //});
            builder.Services.AddHttpContextAccessor();
            builder.Services.Configure<Kundkartan.Shared.Settings.Application>(applicationSettingsSection);
            builder.Services.AddSignalHubServices(applicationSettingsSection);
            builder.Services.AddKundkartanServices(Configuration);

            await builder.Build().RunAsync();
		}

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(s =>
            {
                // Get the service address from appsettings.json
                Configuration = s.GetRequiredService<IConfiguration>();
                var settings = Configuration["ApplicationSettings"];
                return settings;

            });

            var applicationSettingsSection = Configuration.GetSection("ApplicationSettings");
            services.Configure<SignalHub.Shared.Settings.Application>(applicationSettingsSection);
            var isDevEnv = (Environment.GetEnvironmentVariables().Contains("ASPNETCORE_ENVIRONMENT"))
                ? string.Equals(Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT"), "Development", StringComparison.OrdinalIgnoreCase)
                : default;

            // SignalHub general
            if (!isDevEnv) services.AddSignalHub("SignalHub", applicationSettingsSection);

            //services.AddRazorPages();
            //services.AddServerSideBlazor().AddHubOptions(o =>
            //{
            //    o.MaximumReceiveMessageSize = 10 * 1024 * 1024;
            //});
            services.AddHttpContextAccessor();
            services.Configure<Kundkartan.Shared.Settings.Application>(applicationSettingsSection); 
            services.AddSignalHubServices(applicationSettingsSection);
            services.AddKundkartanServices(Configuration);

        }
    }
}
