﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SignalHub.Shared.Extensions
{
    public static class StringExtensions
    {
        public static string ToUri(this string me)
        {
            if (string.IsNullOrWhiteSpace(me))
                return me;

            var uri = new StringBuilder();
            foreach (var c in me.ToLowerInvariant())
            {
                if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || c == '-')
                    uri.Append(c);
            }
            return uri.ToString();
        }

        public static string[] SplitLine(this string value)
        {
            var newLineSplitters = new[] { Environment.NewLine, "\n", "\r" };
            return value.Split(newLineSplitters, StringSplitOptions.None);
        }

        public static string ToCharString(this IEnumerable<char> source)
        {
            return new string(source.ToArray());
        }
    }
}
