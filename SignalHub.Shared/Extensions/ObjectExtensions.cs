﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SignalHub.Shared.Extensions
{
    public static class ObjectExtensions
    {
        public static bool HasValue(this object obj)
        {
            return obj != null;
        }

        public static dynamic ToDynamic<T>(this T obj) //where T : Shared.Models.Entity
        {
            IDictionary<string, object> expando = new System.Dynamic.ExpandoObject();

            foreach (var propertyInfo in typeof(T).GetProperties())
            {
                var currentValue = propertyInfo.GetValue(obj);
                if (propertyInfo.GetCustomAttributes(true).Any(a => a is Newtonsoft.Json.JsonConverterAttribute))
                {
                    var valueType = currentValue.GetType();
                    var typeName = $"{valueType.FullName}, {valueType.Assembly.GetName().Name}";
                    dynamic childObject = ToDynamic(currentValue);
                    childObject.Add("$type", typeName);
                    expando.Add(propertyInfo.Name, childObject);
                }
                else
                    expando.Add(propertyInfo.Name, currentValue);
            }
            return expando as System.Dynamic.ExpandoObject;
        }

        public static T Clone<T>(this T obj)
        {
            var json = System.Text.Json.JsonSerializer.Serialize<T>(obj);
            return System.Text.Json.JsonSerializer.Deserialize<T>(json);
        }

        public static bool IsAllFieldsNullOrEmpty(this object me)
        {
            return me.IsAllFieldsNullOrEmpty(new List<string>());
        }
        
        public static bool IsAllFieldsNullOrEmpty(this object me, IEnumerable<string> exceptList)
        {
            foreach (PropertyInfo pi in me.GetType().GetProperties())
            {
                if (exceptList.Contains(pi.Name))
                    continue;

                object value = pi.GetValue(me);
                if (pi.PropertyType == typeof(string))
                {
                    if (!string.IsNullOrEmpty((string)value))
                        return false;
                }
                else if (pi.PropertyType == typeof(bool))
                {
                    if ((bool)value)
                        return false;
                }
                else if (value != null)
                    return false;
            }
            return true;
        }
    }
}
