﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SignalHub.Shared.Extensions
{
    public static class EnumerableExtensions
    {
        public static int? IndexOf<T>(this IEnumerable<T> items, T value)
        {
            if (items == null || value == null)
                return null;

            var i = 0;
            foreach (var item in items)
            {
                if (item.GetHashCode() == value.GetHashCode())
                    return i;
                i++;
            }
            return null;
        }

        public static bool IsNotEmptyOrNull<T>(this IEnumerable<T> items)
        {
            return items != null && items.Any();
        }

        public static bool IsEmptyOrNull<T>(this IEnumerable<T> items)
        {
            return items == null || items.Count() == 0;
        }

        public static bool IsEmpty<T>(this IEnumerable<T> items)
        {
            return items.Count() == 0;
        }

        public static List<T> WithToList<T>(this IEnumerable<T> items, Action<List<T>> callback)
        {
            var newList = items.ToList();
            callback(newList);
            return newList;
        }

        public static List<T> ToListAndAdd<T>(this IEnumerable<T> items, T additionalItem)
        {
            var newList = items.ToList();
            newList.Add(additionalItem);
            return newList;
        }

        public static List<T> ToListAndAdd<T>(this IEnumerable<T> items, IEnumerable<T> additionalItem)
        {
            var newList = items.ToList();
            newList.AddRange(additionalItem);
            return newList;
        }
    }
}
