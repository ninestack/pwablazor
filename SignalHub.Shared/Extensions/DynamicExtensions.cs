﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;

namespace SignalHub.Shared.Extensions
{
    public static class DynamicExtensions
    {
        public static IDictionary<string, object> ToDictionary(this object me)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(me))
            {
                object obj = propertyDescriptor.GetValue(me);
                dictionary.Add(propertyDescriptor.Name, obj);
            }
            return dictionary;
        }

        public static dynamic ToExpando(this IDictionary<string, object> me)
        {
            dynamic eo = me.Aggregate(new ExpandoObject() as IDictionary<string, Object>,
                            (a, p) => { a.Add(p.Key, p.Value); return a; });

            return eo;
        }
    }
}
