﻿namespace SignalHub.Shared.Loaders
{
    public class FilterField
    {
        public Models.FieldFilter Filter { get; set; }
        public Models.Field Field { get; set; }
    }
}
