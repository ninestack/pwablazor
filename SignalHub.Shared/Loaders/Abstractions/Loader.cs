﻿using SignalHub.Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalHub.Shared.Loaders.Abstractions
{
    public interface IAggregate
    {
        //Task<IDictionary<string, IEnumerable<Models.Responses.KeyValue>>> Aggregate(object payload);
        Task<IEnumerable<T>> Aggregate<T>(object payload);
    }

    public interface ITabular
    {
        Task<IEnumerable<T>> Tabular<T>(object payload);
    }

    public interface IDictinct
    {
        Task<IEnumerable<T>> Distinct<T>(object payload);
    }


    public class FilterChangeEventArgs : EventArgs
    {
        public FilterField CurrentFilter { get; set; }
        public IEnumerable<FilterField> Filters { get; set; }

        public static FilterChangeEventArgs CreateFrom(FilterField filter, IEnumerable<FilterField> filters) =>
            new FilterChangeEventArgs { CurrentFilter = filter, Filters = filters };
        public static FilterChangeEventArgs CreateFrom(IEnumerable<FilterField> filters) =>
            new FilterChangeEventArgs { Filters = filters };
    }

    public class LoadEventArgs : EventArgs
    {
        public string SearchText { get; set; }
        public IEnumerable<FilterField> Filters { get; set; }
    }


    public abstract class Loader : IAggregate, ITabular, IDictinct
    {
        public abstract string Name { get; }
        public abstract IEnumerable<Models.Field> Fields { get; }
        public IEnumerable<FilterField> Filters { get; private set; } = new List<FilterField>();
        public string SearchText { get; private set; }

        public event Func<Loader, FilterChangeEventArgs, Task> OnFilterSet;
        public event Func<Loader, FilterChangeEventArgs, Task> OnFilterRemoved;
        public event Func<Loader, string, Task> OnSearchSet;
        public event Func<Loader, string, Task> OnSearchCleared;
        public event Func<Loader, LoadEventArgs, Task> OnLoad;

        public async Task SetFilters(IEnumerable<FilterField> filters)
        {
            foreach (var filter in filters)
            {
                //var filterExists = Filters.FirstOrDefault(f => f.Filter.Id == filter.Filter.Id);
                var filterExists = GetFilterByIdOrName(filter);
                if (filterExists != null)
                    WithFilters(filters => filters.Remove(filterExists));
                WithFilters(filters => filters.Add(filter));
            }
            var allFilters = new List<FilterField>().AddMany(filters, Filters);
            var filterChangeArgs = FilterChangeEventArgs.CreateFrom(allFilters);
            await HandleEvent(OnFilterSet, filterChangeArgs);
            await FireLoadEvent();
        }

        public async Task SetFilter(FilterField filter)
        {
            //var filterExists = Filters.FirstOrDefault(f => f.Filter.Id == filter.Filter.Id);
            var filterExists = GetFilterByIdOrName(filter);
            if (filterExists != null)
                WithFilters(filters => filters.Remove(filterExists));
            WithFilters(filters => filters.Add(filter));

            var filterChangeArgs = FilterChangeEventArgs.CreateFrom(filter, Filters);
            await HandleEvent(OnFilterSet, filterChangeArgs);
            await FireLoadEvent();
        }

        public async Task RemoveFilter(FilterField filter)
        {
            //var filterToRemove = Filters.FirstOrDefault(f => f.Filter.Id == filter.Filter.Id);
            var filterToRemove = GetFilterByIdOrName(filter);
            if (filterToRemove == null)
                return;
            WithFilters(filters => filters.Remove(filterToRemove));

            var filterChangeArgs = FilterChangeEventArgs.CreateFrom(filter, Filters);
            await HandleEvent(OnFilterRemoved, filterChangeArgs);
            await FireLoadEvent();
        }

        private FilterField GetFilterByIdOrName(FilterField filter)
        {
            return string.IsNullOrWhiteSpace(filter.Filter.Id) ? Filters.FirstOrDefault(f => f.Filter.Name == filter.Filter.Name) : Filters.FirstOrDefault(f => f.Filter.Id == filter.Filter.Id);
        }

        public async Task SetSearch(string searchText)
        {
            SearchText = searchText;
            await HandleEvent(OnSearchSet, searchText);
            //var filterChangeArgs = FilterChangeEventArgs.CreateFrom(Filters);
            //await HandleEvent(OnFilterSet, filterChangeArgs);
            await FireLoadEvent();
        }
        public async Task ClearSearch(string searchText)
        {
            SearchText = null;
            await HandleEvent(OnSearchCleared, (string)null);
            await FireLoadEvent();
        }

        private async Task FireLoadEvent()
        {
            await HandleEvent(OnLoad, new LoadEventArgs { Filters = Filters, SearchText = SearchText });
        }

        private async Task HandleEvent<E>(Func<Loader, E, Task> handler, E args)
        {
            if (handler == null)
            {
                return;
            }
            Delegate[] invocationList = handler.GetInvocationList();
            Task[] handlerTasks = new Task[invocationList.Length];

            for (int i = 0; i < invocationList.Length; i++)
            {
                handlerTasks[i] = ((Func<Loader, E, Task>)invocationList[i])(this, args);
            }
            await Task.WhenAll(handlerTasks);
        }

        private void WithFilters(Action<IList<FilterField>> callback)
        {
            var filters = Filters.ToList();
            callback(filters);
            Filters = filters;
        }

        public abstract Task<double?> Metric(object payload);

        public abstract Task<IEnumerable<T>> Aggregate<T>(object payload);

        public abstract Task<IEnumerable<T>> Tabular<T>(object payload);

        public abstract Task<IEnumerable<T>> Distinct<T>(object payload);
    }
}
