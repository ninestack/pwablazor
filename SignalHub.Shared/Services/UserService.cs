﻿using SignalHub.Shared.Extensions;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace SignalHub.Shared.Services
{
    public class UserService
    {
        private HttpClient httpClient;

        public UserService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        #region User
        public async Task<Models.User> GetByEmail(string email)
        {
            var uri = $"user/email/{email}";
            return await httpClient.GetJsonSignalHubAsync<Models.User>(uri);
        }
        #endregion

        #region Latest sources
        public async Task<IEnumerable<Models.Source>> GetLatestSources(string userId, int count = 3)
        {
            var uri = $"user/source/latest/{userId}?count={count}";
            return await httpClient.GetJsonSignalHubAsync<IEnumerable<Models.Source>>(uri);
        }

        public async Task AppendLatestSource(string sourceId, string userId)
        {
            var uri = $"user/source/latest";
            await httpClient.PostJsonSignalHubAsync<Models.Visual>(uri, new { sourceId, userId });
        }
        #endregion

        #region Starred sources
        public async Task<IEnumerable<Models.Source>> GetStarredSources(string userId, int count = 3)
        {
            var uri = $"user/source/starred/{userId}";
            return await httpClient.GetJsonSignalHubAsync<IEnumerable<Models.Source>>(uri);
        }

        public async Task AppendStarredSources(string sourceId, string userId)
        {
            var uri = $"user/source/starred";
            await httpClient.PostJsonSignalHubAsync<Models.Visual>(uri, new { sourceId, userId });
        }
        #endregion

        #region Latest visuals
        public async Task<IEnumerable<Models.Visual>> GetLatestVisuals(string userId, int count = 3)
        {
            var uri = $"user/visual/latest/{userId}?count={count}";
            return await httpClient.GetJsonSignalHubAsync<IEnumerable<Models.Visual>>(uri);
        }

        public async Task AppendLatestVisual(string visualId, string userId)
        {
            var uri = $"user/visual/latest";
            await httpClient.PostJsonSignalHubAsync<Models.Visual>(uri, new { visualId, userId });
        }
        #endregion
    }
}
