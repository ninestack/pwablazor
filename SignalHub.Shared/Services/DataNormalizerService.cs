﻿using SignalHub.Shared.Models;
using System.Collections.Generic;
using System.Linq;

namespace SignalHub.Shared.Services
{
    public class DataNormalizerService
    {
        //public IEnumerable<object> NormalizeSingleDimension(IEnumerable<dynamic> signalHubAggregationData)
        //{
        //    return signalHubAggregationData;
        //    var result = new List<Dictionary<string, object>>();
        //    foreach (var keyValue in signalHubAggregationData)
        //    {
        //        var key = TryGetValue(keyValue.key) as string;
        //        var value0 = TryGetValue(keyValue.value0);
        //        var value1 = TryGetValue(keyValue.value1);
        //        var value2 = TryGetValue(keyValue.value2);
        //        var value3 = TryGetValue(keyValue.value3);
        //        var value4 = TryGetValue(keyValue.value4);

        //        Dictionary<string, object> item = result.FirstOrDefault(r => r.ContainsKey("key") && r.ContainsValue(keyValue.key));
        //        if (item == null)
        //        {
        //            item = new Dictionary<string, object>();
        //            result.Add(item);
        //            item["key"] = key;
        //        }
        //        item[$"value0"] = value0;
        //        item[$"value1"] = value1;
        //        item[$"value2"] = value2;
        //        item[$"value3"] = value3;
        //        item[$"value4"] = value4;
        //        result.Add(item);
        //    }
        //    return result;
        //}

        public IEnumerable<object> NormalizeMultiDimension(IEnumerable<dynamic> signalHubAggregationData, IEnumerable<FieldDimension> dimensions, IEnumerable<FieldMeasure> measures)
        {
            //return signalHubAggregationData;

            var dimensionTitles = new List<string>();


            var result = new List<Dictionary<string, object>>();
            var convertedData = signalHubAggregationData.Select(a => CreateKeyValueFrom(a));
            //foreach (var keyValue in signalHubAggregationData)
            foreach (var keyValue in convertedData.OrderBy(a => a.key).ThenBy(a => a.value1))
            {
#pragma warning disable CS0219 // The variable 'values' is assigned but its value is never used
                var values = 0;
#pragma warning restore CS0219 // The variable 'values' is assigned but its value is never used
#pragma warning disable CS0219 // The variable 'names' is assigned but its value is never used
                var names = 0;
#pragma warning restore CS0219 // The variable 'names' is assigned but its value is never used
                //var key = TryGetValue(keyValue.key);
                //var value1 = TryGetValue(keyValue.value1);
                Dictionary<string, object> row = null;
                foreach (var item in result)
                {
                    if (item.ContainsKey("key") && item.ContainsValue(keyValue.key))
                    {
                        row = item;
                        break;
                    }
                }
                if (row == null)
                {
                    row = new Dictionary<string, object>();
                    row["key"] = keyValue.key;
                    //row["name"] = keyValue.value1;
                    result.Add(row);
                }
                //var value = TryGetValue(keyValue.value0);
                var rowCount = row.Where(r => r.Key.StartsWith("value")).Count();
                row[$"value{rowCount}"] = keyValue.value0;
                row[$"name{rowCount}"] = keyValue.value1;


                //row[$"value{values++}"] = keyValue.value0;
                //row[$"name{names++}"] = keyValue.value1;


                //var value0 = TryGetValue(keyValue.value0); // Date
                //var value1 = TryGetValue(keyValue.value1);
                //var value2 = TryGetValue(keyValue.value2);
                //var value3 = TryGetValue(keyValue.value3);
                //var value4 = TryGetValue(keyValue.value4);
                ////Dictionary<string, object> value = new Dictionary<string, object>(); // GetValueFromKey(result, keyValue.key);
                //row[$"value0"] = value0;
                //row[$"value1"] = value1;
                //row[$"value2"] = value2;
                //row[$"value3"] = value3;
                //row[$"value4"] = value4;
            }
            return result;
        }

        private Models.Responses.KeyValue CreateKeyValueFrom(Models.Responses.KeyValue keyValue)
        {
            //var items = new Dictionary<string, object>();
            var item = new Models.Responses.KeyValue();
            item.key = TryGetValue(keyValue.key);
            item.value0 = TryGetValue(keyValue.value0);
            item.value1 = TryGetValue(keyValue.value1);
            item.value2 = TryGetValue(keyValue.value2);
            item.value3 = TryGetValue(keyValue.value3);
            item.value4 = TryGetValue(keyValue.value4);
            return item;
        }

        private static object TryGetValue(object value)
        {
            if (value == null)
                return value;

            if (value is System.Text.Json.JsonElement)
            {
                var jsonElement = (System.Text.Json.JsonElement)value;
                switch (jsonElement.ValueKind)
                {
                    case System.Text.Json.JsonValueKind.True:
                    case System.Text.Json.JsonValueKind.False:
                        return jsonElement.GetBoolean();
                    case System.Text.Json.JsonValueKind.Number:
                        return jsonElement.GetDouble();
                    case System.Text.Json.JsonValueKind.String:
                        return jsonElement.GetString();
                    default:
                        return jsonElement.GetRawText();
                }
            }
            return value;
        }

        private static Dictionary<string, object> GetValueFromKey(List<Dictionary<string, object>> list, object value)
        {
            foreach (var item in list)
                if (item.ContainsKey("key") && item.ContainsValue(value))
                    return item;
            return null;
        }

        private static Dictionary<object, object> ConvertDynamicToDictonary(IDictionary<string, object> value)
        {
            var newDict = new Dictionary<object, object>();
            foreach (var item in value)
                newDict.Add(item.Key, item.Value);

            return ConvertDynamicToDictonary(newDict);
        }

        private static Dictionary<object, object> ConvertDynamicToDictonary(IDictionary<object, object> value)
        {
            return value.ToDictionary(
                p => p.Key,
                p =>
                {
                    // if it's another IDict (might be a ExpandoObject or could also be an actual Dict containing ExpandoObjects) just go trough it recursively
                    if (p.Value is IDictionary<object, object> dict)
                    {
                        return ConvertDynamicToDictonary(dict);
                    }
                    // if it's an IEnumerable, it might have ExpandoObjects inside, so check for that
                    if (p.Value is IEnumerable<object> list)
                    {
                        if (list.Any(o => o is System.Dynamic.ExpandoObject))
                        {
                            // if it does contain ExpandoObjects, take all of those and also go trough them recursively
                            return list
                                .Where(o => o is System.Dynamic.ExpandoObject)
                                .Select(o => ConvertDynamicToDictonary((System.Dynamic.ExpandoObject)o));
                        }
                    }
                    // neither an IDict nor an IEnumerable -> it's probably fine to just return the value it has
                    return p.Value;
                }
            );
        }
    }
}
