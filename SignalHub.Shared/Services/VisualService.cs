﻿using SignalHub.Shared.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SignalHub.Shared.Services
{
    public class VisualService
    {
        private HttpClient httpClient;

        public VisualService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        #region Visual retrieve
        //public Models.Visual CreateEmpty(string sourceId, string defaultType = "Metric")
        //{
        //    var drilldownTypes = GetDrilldownTypes();
        //    var defaultDrilldown = drilldownTypes.FirstOrDefault(d => d.Name == defaultType);
        //    if (defaultDrilldown == null)
        //        defaultDrilldown = drilldownTypes.FirstOrDefault(d => d.IsDefualt);

        //    var visual = new Models.Visual
        //    {
        //        Id = Guid.NewGuid().ToString(),
        //        Title = string.Empty,
        //        Type = defaultDrilldown.Name,
        //        SourceId = sourceId,
        //        Config = new Models.QueryConfig()
        //    };
        //    //visual.Config.MeasureFields = new List<Models.FieldMeasure> { defaultDrilldown.DefaultMeasureField };
        //    return visual;
        //}

        public async Task<Models.Visual> Get(string visualId)
        {
            var uri = $"visual/{visualId}";
            return await httpClient.GetJsonSignalHubAsync<Models.Visual>(uri);
        }

        public async Task<IEnumerable<Models.Visual>> GetBySource(string sourceId)
        {
            var uri = $"visual/source/{sourceId}";
            return await httpClient.GetJsonSignalHubAsync<IEnumerable<Models.Visual>>(uri);
        }

        public async Task<IEnumerable<Models.Visual>> Get(IEnumerable<string> visualIds)
        {
            var uri = $"visual/multi";
            return await httpClient.GetJsonSignalHubAsync<IEnumerable<Models.Visual>>(uri, new { visualIds });
        }
        #endregion

        #region Visual upsert
        public async Task<Models.Visual> Upsert(Models.Visual visual)
        {
            var uri = $"visual";
            return await httpClient.PostJsonSignalHubAsync<Models.Visual>(uri, visual);
        }
        #endregion

        #region Visual add to application and dashboard
#pragma warning disable CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        public async Task UpdateLinkApplications(string visualId, IEnumerable<string> added, IEnumerable<string> removed)
#pragma warning restore CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        {

        }
        #endregion

    }
}
