﻿using SignalHub.Shared.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SignalHub.Shared.Services
{
    public class SourceService
    {
        private HttpClient httpClient;

        public SourceService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        #region Source
        public async Task<IEnumerable<Models.Source>> All()
        {
            var uri = "source";
            return await httpClient.GetJsonSignalHubAsync<IEnumerable<Models.Source>>(uri);
        }

        public async Task<Models.Source> Get(string id)
        {
            var uri = $"source/{id}";
            return await httpClient.GetJsonSignalHubAsync<Models.Source>(uri);
        }

        public async Task<IEnumerable<Models.Source>> Get(IEnumerable<string> sourceIds)
        {
            var uri = $"source/multi";
            return await httpClient.GetJsonSignalHubAsync<IEnumerable<Models.Source>>(uri, new { sourceIds = sourceIds.Distinct() });
        }

        public async Task<Models.Source> GetByUrl(string url)
        {
            var uri = $"source/url/{url}";
            return await httpClient.GetJsonSignalHubAsync<Models.Source>(uri);
        }

        public async Task<Models.Source> Upsert(Models.Source source)
        {
            var uri = $"source";
            return await httpClient.PostJsonSignalHubAsync<Models.Source>(uri, source);
        }
        #endregion

        #region Source metadata
        public async Task<Models.SourceMetadata> GetMetadata(string sourceId)
        {
            var uri = $"source/{sourceId}/metadata";
            var meta = await httpClient.GetJsonSignalHubAsync<Models.SourceMetadata>(uri);
            return meta;
        }

        public async Task<bool> UpdateMetadata(Models.SourceMetadata metadata)
        {
            var uri = $"source/{metadata.SourceId}/metadata";
            var response = await httpClient.PostJsonSignalHubAsync<Models.SourceMetadata>(uri, metadata);
            return true;
        }
        #endregion

        #region Mapping
        public async Task<bool> UpdateMapping(Models.Source source)
        {
            var mappingUri = $"{source.Url}/mapping";
            var fieldMappings = await httpClient.GetJsonSignalHubAsync<IEnumerable<Models.Field>>(mappingUri);
            source.Fields = MergeFields(fieldMappings, source.Fields);
            await Upsert(source);
            return true;
        }

        private IEnumerable<Models.Field> MergeFields(IEnumerable<Models.Field> fieldMappings, IEnumerable<Models.Field> existingFields)
        {
            var mergedFields = new List<Models.Field>();
            foreach (var field in fieldMappings)
            {
                var existingField = existingFields.FirstOrDefault(f => f.Name == field.Name);
                if (existingField.HasValue())
                {
                    existingField.DataType = field.DataType;
                    if (!existingField.TypeChangedByUser)
                        existingField.Type = field.Type;
                }
                else
                    existingField = field;

                mergedFields.Add(existingField);
            }
            return mergedFields;
        }
        #endregion
    }
}
