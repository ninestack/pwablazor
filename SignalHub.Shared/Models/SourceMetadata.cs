﻿using System;
using System.Collections.Generic;

namespace SignalHub.Shared.Models
{
    public class SourceMetadata : Abstractions.IHubEntity
    {
        public string __type { get; } = "sourcemetadata";
        public string Id { get; set; }
        public string SourceId { get; set; }
        public string SourceName { get; set; }

        public string Creator { get; set; }
        public string Owner { get; set; }
        public string LegalOwner { get; set; }
        //public string LastUpdatedBy { get; set; }
        public bool Purchased { get; set; }
        public string DataSupplier { get; set; }

        public DateTime? Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Archived { get; set; }
        public string ArchivedBy { get; set; }
        public DateTime? ArchivedAt { get; set; }

        public IList<SourceDictionary> SourceDictionaries { get; set; } = new List<SourceDictionary>();
        public IList<SourceFieldsDictionary> FieldDictionaries { get; set; } = new List<SourceFieldsDictionary>();
    }

    public class SourceDictionary
    {
        public string Language { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        //public DateTime? Updated { get; set; }
        public string DescriptionInternal { get; set; }
        public string UpdateFrequency { get; set; }
        public string Access { get; set; }
        public string Processed { get; set; }
        public string ProcessedDependies { get; set; }
        public string Country { get; set; }
        public string Branch { get; set; }
        public string Storage { get; set; }
        public bool GDPRSensitive { get; set; }
        public string GDPRPurpose { get; set; }
        public string GDPRLegal { get; set; }
        public string GDPROutput { get; set; }
        public string GDPRAutomatic { get; set; }
        public string GDPRCategorization { get; set; }
        public string GDPRAnonFreq { get; set; }
        public string GDPRAnonExpire { get; set; }

        public string PIARisk { get; set; }
        public string PIAImpact { get; set; }
        public string PIAComment { get; set; }
    }
    public class SourceFieldsDictionary
    {
        public string Language { get; set; }
        public IList<FieldMetadata> Fields { get; set; } = new List<FieldMetadata>();
    }
}
