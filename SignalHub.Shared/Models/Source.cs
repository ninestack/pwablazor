﻿using System;
using System.Collections.Generic;

namespace SignalHub.Shared.Models
{
    public class Source : Abstractions.IHubEntity
    {
        public string __type { get; } = "source";
        public string Id { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public QueryEngine Engine { get; set; }
        public string Index { get; set; }
        public string TypeName { get; set; } = "data";
        public string Database { get; set; }
        public string Table { get; set; }
        public string Template { get; set; }
        public IEnumerable<int> GrantedAccessCompanies { get; set; } = new List<int>();
        public IEnumerable<Field> Fields { get; set; } = new List<Field>();
        public IEnumerable<Variable> Variables { get; set; } = new List<Variable>();

        public DateTime? DataCreatedAt { get; set; }
        public DateTime? DataUpdatedAt { get; set; }
        public string DataUpdatedBy { get; set; }
        public long? DataRecordCount { get; set; }
        public int? CacheSeconds { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Archived { get; set; }
        public string ArchivedBy { get; set; }
        public DateTime? ArchivedAt { get; set; }
    }

    public class QueryEngine
    {
        public enum EngineTypeDefinition { Elastic, Kusto }

        public EngineTypeDefinition EngineType { get; set; } = EngineTypeDefinition.Kusto;
        public string Title { get; set; }
    }
}
