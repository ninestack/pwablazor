﻿namespace SignalHub.Shared.Models
{
    public class Layout
    {
        public Position Position { get; set; }
        public string VisualId { get; set; }
    }

    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int H { get; set; }
        public int W { get; set; }
    }
}
