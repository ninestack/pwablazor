﻿using System.Collections.Generic;

namespace SignalHub.Shared.Models
{
    public class Variable
    {
        public enum TypeDefinition { Number, String }

        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TypeDefinition Type { get; set; } = TypeDefinition.String;
        public object Value { get; set; }
        public object DefaultValue { get; set; }
        public bool IsFixedValues { get; set; }
        public IEnumerable<VariableIdTitle> FixedValues { get; set; }
    }

    public class VariableIdTitle
    {
        public object Id { get; set; }
        public string Title { get; set; }
    }
}
