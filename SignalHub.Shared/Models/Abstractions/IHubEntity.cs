﻿using System;

namespace SignalHub.Shared.Models.Abstractions
{
    public interface IHubEntity
    {
        string __type { get; }
        string Id { get; set; }

        string CreatedBy { get; set; }
        DateTime? CreatedAt { get; set; }
        string UpdatedBy { get; set; }
        DateTime? UpdatedAt { get; set; }
        bool Archived { get; set; }
        string ArchivedBy { get; set; }
        DateTime? ArchivedAt { get; set; }
    }
}
