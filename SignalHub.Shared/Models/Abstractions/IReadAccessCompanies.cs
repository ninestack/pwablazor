﻿using System.Collections.Generic;

namespace SignalHub.Shared.Models.Abstractions
{
    public interface IReadAccessCompanies
    {
        IEnumerable<string> GrantedAccessCompanies { get; set; }
    }
}
