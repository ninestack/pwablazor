﻿using System;
using System.Collections.Generic;

namespace SignalHub.Shared.Models
{
    public class Dashboard : Abstractions.IHubEntity
    {
        public string __type { get; } = "dashboard";
        public string Id { get; set; }
        public string Uri { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public IList<Visual> Visuals { get; set; } = new List<Visual>();
        public IList<Layout> Layouts { get; set; } = new List<Layout>();

        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Archived { get; set; }
        public string ArchivedBy { get; set; }
        public DateTime? ArchivedAt { get; set; }
    }
}
