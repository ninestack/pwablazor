﻿using System;

namespace SignalHub.Shared.Models
{
    public class Visual : Abstractions.IHubEntity
    {
        public string __type { get; } = "visual";
        public string Id { get; set; }
        public string Title { get; set; }
        public string SourceId { get; set; }
        public string Type { get; set; }
        public QueryConfig Config { get; set; } = new QueryConfig();
        [System.Text.Json.Serialization.JsonConverter(typeof(JsonConverters.BaseClassObjectConverter<VisualAppearance>))]
        [Newtonsoft.Json.JsonProperty(TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Auto)]
        //[Newtonsoft.Json.JsonConverter(typeof(JsonConverters.NewtonsoftConvertStandard))]
        public VisualAppearance Appearance { get; set; }
        public int Version { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Archived { get; set; }
        public string ArchivedBy { get; set; }
        public DateTime? ArchivedAt { get; set; }
    }
}
