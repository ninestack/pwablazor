﻿using System;
using System.Collections.Generic;

namespace SignalHub.Shared.Models
{
    public class Company : Abstractions.IHubEntity
    {
        public string __type { get; } = "company";
        public string Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string AppId { get; set; }
        public string ApiSecret { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Archived { get; set; }
        public string ArchivedBy { get; set; }
        public DateTime? ArchivedAt { get; set; }

    }

    public class User : Abstractions.IHubEntity
    {
        public string __type { get; } = "user";
        public string Id { get; set; }
        public string CompanyId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        //public string ExternalId { get; set; }
        public string Language { get; set; }
        public IList<SourceInfo> StarredSources { get; set; } = new List<SourceInfo>();
        public IList<SourceInfo> LatestSources { get; set; } = new List<SourceInfo>();
        public IList<VisualInfo> LatestVisuals { get; set; } = new List<VisualInfo>();

        public string CreatedBy { get; set; }
        public DateTime? CreatedAt { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public bool Archived { get; set; }
        public string ArchivedBy { get; set; }
        public DateTime? ArchivedAt { get; set; }

    }

    public class SourceInfo
    {
        public string Id { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
    }

    public class VisualInfo
    {
        public string Id { get; set; }
        public string Title { get; set; }
    }
}
