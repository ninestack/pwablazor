﻿using System.Collections.Generic;

namespace SignalHub.Shared.Models
{
    public class VisualAppearance
    {
        public virtual int Rows { get; set; } = 100;
        public string ThemeName { get; set; } = string.Empty;
        public string ClassName { get; set; } = string.Empty;
    }

    #region Table
    public class TableAppearance : VisualAppearance
    {
        public override int Rows { get; set; } = 1000;
        public int DefaultNumberOfDecimals { get; set; } = 2;
        public IList<TableFieldAppearance> TableFields { get; set; } = new List<TableFieldAppearance>();
    }
    public class TableFieldAppearance
    {
        public string Name { get; set; }
        public int? CellWidth { get; set; }
        public int? Index { get; set; }
        public bool Hidden { get; set; }
    }
    #endregion

    #region Graph
    public class GraphAppearance : VisualAppearance
    {
        public override int Rows { get; set; } = 50;
        public string Theme { get; set; }
        public int ThemeVariant { get; set; } = 1;
        public int DonutRadius { get; set; }
        public IList<MeasureGraphAppearance> Measures { get; set; } = new List<MeasureGraphAppearance>();
        public IList<DimensionGraphAppearance> Dimensions { get; set; } = new List<DimensionGraphAppearance>();
        //public string CategoryAxisTitle { get; set; }
        public string ValueAxisTitle { get; set; }
        public string LegendPosition { get; set; }
        public string LabelPosition { get; set; }
        public string LabelColor { get; set; } = "black";
        public int LabelFontSize { get; set; } = 15;
        public string LabelFontWeight { get; set; } = "bold";
        public string LabelBulletPosition { get; set; }
        public string LabelBulletColor { get; set; }
        public string NumberFormat { get; set; }
        public bool Stacked { get; set; }
        public bool Percentage { get; set; }
        public long? MinValue { get; set; }
    }
    public class MeasureGraphAppearance
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string SeriesType { get; set; }
    }
    public class DimensionGraphAppearance
    {
        public string Name { get; set; }
        public string Title { get; set; }
    }
    #endregion

    #region Metric
    public class MetricAppearance : VisualAppearance
    {
        public enum OrientationDefinition { Vertical, Horizontal }
        public enum IconPlacementDefinition { Left, Right }

        public OrientationDefinition Orientation { get; set; } = OrientationDefinition.Vertical;
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Icon { get; set; }
        public IconPlacementDefinition IconPlacement { get; set; } = IconPlacementDefinition.Left;
        public override int Rows { get; set; } = 1;
    }
    #endregion

    #region Map
    public class MapAppearance : VisualAppearance
    {
        public override int Rows { get; set; } = 500;
        public string SourceId { get; set; }
        public string IdField { get; set; }
        public string PolygonField { get; set; }
    }
    #endregion
}
