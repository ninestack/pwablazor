﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace SignalHub.Shared.Collections
{
    public class ObservableCollectionEx<T> : ObservableCollection<T>
    {
        //private bool _notificationSupressed = false;
        private bool _supressNotification = false;

        public void SupressNotificationWhen(Action<ObservableCollectionEx<T>> callback)
        {
            _supressNotification = true;
            callback(this);
            _supressNotification = false;
        }

        //public bool SupressNotification
        //{
        //    get
        //    {
        //        return _supressNotification;
        //    }
        //    set
        //    {
        //        _supressNotification = value;
        //        if (_supressNotification == false && _notificationSupressed)
        //        {
        //            this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        //            _notificationSupressed = false;
        //        }
        //    }
        //}

        protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            if (_supressNotification)
            {
                //_notificationSupressed = true;
                return;
            }
            base.OnCollectionChanged(e);
        }
    }
}
